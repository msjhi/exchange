package sn.mara.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.mara.web.rest.TestUtil;

public class ExchangeValueDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangeValueDTO.class);
        ExchangeValueDTO exchangeValueDTO1 = new ExchangeValueDTO();
        exchangeValueDTO1.setId(1L);
        ExchangeValueDTO exchangeValueDTO2 = new ExchangeValueDTO();
        assertThat(exchangeValueDTO1).isNotEqualTo(exchangeValueDTO2);
        exchangeValueDTO2.setId(exchangeValueDTO1.getId());
        assertThat(exchangeValueDTO1).isEqualTo(exchangeValueDTO2);
        exchangeValueDTO2.setId(2L);
        assertThat(exchangeValueDTO1).isNotEqualTo(exchangeValueDTO2);
        exchangeValueDTO1.setId(null);
        assertThat(exchangeValueDTO1).isNotEqualTo(exchangeValueDTO2);
    }
}
