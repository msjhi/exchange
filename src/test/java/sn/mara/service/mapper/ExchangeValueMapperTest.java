package sn.mara.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class ExchangeValueMapperTest {

    private ExchangeValueMapper exchangeValueMapper;

    @BeforeEach
    public void setUp() {
        exchangeValueMapper = new ExchangeValueMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(exchangeValueMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(exchangeValueMapper.fromId(null)).isNull();
    }
}
