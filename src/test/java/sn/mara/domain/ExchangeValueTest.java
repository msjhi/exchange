package sn.mara.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.mara.web.rest.TestUtil;

public class ExchangeValueTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExchangeValue.class);
        ExchangeValue exchangeValue1 = new ExchangeValue();
        exchangeValue1.setId(1L);
        ExchangeValue exchangeValue2 = new ExchangeValue();
        exchangeValue2.setId(exchangeValue1.getId());
        assertThat(exchangeValue1).isEqualTo(exchangeValue2);
        exchangeValue2.setId(2L);
        assertThat(exchangeValue1).isNotEqualTo(exchangeValue2);
        exchangeValue1.setId(null);
        assertThat(exchangeValue1).isNotEqualTo(exchangeValue2);
    }
}
