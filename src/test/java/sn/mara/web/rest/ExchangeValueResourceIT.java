package sn.mara.web.rest;

import sn.mara.ExchangeApp;
import sn.mara.config.SecurityBeanOverrideConfiguration;
import sn.mara.domain.ExchangeValue;
import sn.mara.repository.ExchangeValueRepository;
import sn.mara.service.ExchangeValueService;
import sn.mara.service.dto.ExchangeValueDTO;
import sn.mara.service.mapper.ExchangeValueMapper;
import sn.mara.web.rest.errors.ExceptionTranslator;
import sn.mara.service.dto.ExchangeValueCriteria;
import sn.mara.service.ExchangeValueQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static sn.mara.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ExchangeValueResource} REST controller.
 */
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, ExchangeApp.class})
public class ExchangeValueResourceIT {

    private static final String DEFAULT_CURRENCY_FROM = "AAAAAAAAAA";
    private static final String UPDATED_CURRENCY_FROM = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENCY_TO = "AAAAAAAAAA";
    private static final String UPDATED_CURRENCY_TO = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_CONVERSION_MULTIPLE = new BigDecimal(1);
    private static final BigDecimal UPDATED_CONVERSION_MULTIPLE = new BigDecimal(2);
    private static final BigDecimal SMALLER_CONVERSION_MULTIPLE = new BigDecimal(1 - 1);

    @Autowired
    private ExchangeValueRepository exchangeValueRepository;

    @Autowired
    private ExchangeValueMapper exchangeValueMapper;

    @Autowired
    private ExchangeValueService exchangeValueService;

    @Autowired
    private ExchangeValueQueryService exchangeValueQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restExchangeValueMockMvc;

    private ExchangeValue exchangeValue;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ExchangeValueResource exchangeValueResource = new ExchangeValueResource(exchangeValueService, exchangeValueQueryService);
        this.restExchangeValueMockMvc = MockMvcBuilders.standaloneSetup(exchangeValueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangeValue createEntity(EntityManager em) {
        ExchangeValue exchangeValue = new ExchangeValue()
            .currencyFrom(DEFAULT_CURRENCY_FROM)
            .currencyTo(DEFAULT_CURRENCY_TO)
            .conversionMultiple(DEFAULT_CONVERSION_MULTIPLE);
        return exchangeValue;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExchangeValue createUpdatedEntity(EntityManager em) {
        ExchangeValue exchangeValue = new ExchangeValue()
            .currencyFrom(UPDATED_CURRENCY_FROM)
            .currencyTo(UPDATED_CURRENCY_TO)
            .conversionMultiple(UPDATED_CONVERSION_MULTIPLE);
        return exchangeValue;
    }

    @BeforeEach
    public void initTest() {
        exchangeValue = createEntity(em);
    }

    @Test
    @Transactional
    public void createExchangeValue() throws Exception {
        int databaseSizeBeforeCreate = exchangeValueRepository.findAll().size();

        // Create the ExchangeValue
        ExchangeValueDTO exchangeValueDTO = exchangeValueMapper.toDto(exchangeValue);
        restExchangeValueMockMvc.perform(post("/api/exchange-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(exchangeValueDTO)))
            .andExpect(status().isCreated());

        // Validate the ExchangeValue in the database
        List<ExchangeValue> exchangeValueList = exchangeValueRepository.findAll();
        assertThat(exchangeValueList).hasSize(databaseSizeBeforeCreate + 1);
        ExchangeValue testExchangeValue = exchangeValueList.get(exchangeValueList.size() - 1);
        assertThat(testExchangeValue.getCurrencyFrom()).isEqualTo(DEFAULT_CURRENCY_FROM);
        assertThat(testExchangeValue.getCurrencyTo()).isEqualTo(DEFAULT_CURRENCY_TO);
        assertThat(testExchangeValue.getConversionMultiple()).isEqualTo(DEFAULT_CONVERSION_MULTIPLE);
    }

    @Test
    @Transactional
    public void createExchangeValueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = exchangeValueRepository.findAll().size();

        // Create the ExchangeValue with an existing ID
        exchangeValue.setId(1L);
        ExchangeValueDTO exchangeValueDTO = exchangeValueMapper.toDto(exchangeValue);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExchangeValueMockMvc.perform(post("/api/exchange-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(exchangeValueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ExchangeValue in the database
        List<ExchangeValue> exchangeValueList = exchangeValueRepository.findAll();
        assertThat(exchangeValueList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllExchangeValues() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList
        restExchangeValueMockMvc.perform(get("/api/exchange-values?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exchangeValue.getId().intValue())))
            .andExpect(jsonPath("$.[*].currencyFrom").value(hasItem(DEFAULT_CURRENCY_FROM)))
            .andExpect(jsonPath("$.[*].currencyTo").value(hasItem(DEFAULT_CURRENCY_TO)))
            .andExpect(jsonPath("$.[*].conversionMultiple").value(hasItem(DEFAULT_CONVERSION_MULTIPLE.intValue())));
    }
    
    @Test
    @Transactional
    public void getExchangeValue() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get the exchangeValue
        restExchangeValueMockMvc.perform(get("/api/exchange-values/{id}", exchangeValue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(exchangeValue.getId().intValue()))
            .andExpect(jsonPath("$.currencyFrom").value(DEFAULT_CURRENCY_FROM))
            .andExpect(jsonPath("$.currencyTo").value(DEFAULT_CURRENCY_TO))
            .andExpect(jsonPath("$.conversionMultiple").value(DEFAULT_CONVERSION_MULTIPLE.intValue()));
    }


    @Test
    @Transactional
    public void getExchangeValuesByIdFiltering() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        Long id = exchangeValue.getId();

        defaultExchangeValueShouldBeFound("id.equals=" + id);
        defaultExchangeValueShouldNotBeFound("id.notEquals=" + id);

        defaultExchangeValueShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultExchangeValueShouldNotBeFound("id.greaterThan=" + id);

        defaultExchangeValueShouldBeFound("id.lessThanOrEqual=" + id);
        defaultExchangeValueShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllExchangeValuesByCurrencyFromIsEqualToSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where currencyFrom equals to DEFAULT_CURRENCY_FROM
        defaultExchangeValueShouldBeFound("currencyFrom.equals=" + DEFAULT_CURRENCY_FROM);

        // Get all the exchangeValueList where currencyFrom equals to UPDATED_CURRENCY_FROM
        defaultExchangeValueShouldNotBeFound("currencyFrom.equals=" + UPDATED_CURRENCY_FROM);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByCurrencyFromIsNotEqualToSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where currencyFrom not equals to DEFAULT_CURRENCY_FROM
        defaultExchangeValueShouldNotBeFound("currencyFrom.notEquals=" + DEFAULT_CURRENCY_FROM);

        // Get all the exchangeValueList where currencyFrom not equals to UPDATED_CURRENCY_FROM
        defaultExchangeValueShouldBeFound("currencyFrom.notEquals=" + UPDATED_CURRENCY_FROM);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByCurrencyFromIsInShouldWork() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where currencyFrom in DEFAULT_CURRENCY_FROM or UPDATED_CURRENCY_FROM
        defaultExchangeValueShouldBeFound("currencyFrom.in=" + DEFAULT_CURRENCY_FROM + "," + UPDATED_CURRENCY_FROM);

        // Get all the exchangeValueList where currencyFrom equals to UPDATED_CURRENCY_FROM
        defaultExchangeValueShouldNotBeFound("currencyFrom.in=" + UPDATED_CURRENCY_FROM);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByCurrencyFromIsNullOrNotNull() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where currencyFrom is not null
        defaultExchangeValueShouldBeFound("currencyFrom.specified=true");

        // Get all the exchangeValueList where currencyFrom is null
        defaultExchangeValueShouldNotBeFound("currencyFrom.specified=false");
    }
                @Test
    @Transactional
    public void getAllExchangeValuesByCurrencyFromContainsSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where currencyFrom contains DEFAULT_CURRENCY_FROM
        defaultExchangeValueShouldBeFound("currencyFrom.contains=" + DEFAULT_CURRENCY_FROM);

        // Get all the exchangeValueList where currencyFrom contains UPDATED_CURRENCY_FROM
        defaultExchangeValueShouldNotBeFound("currencyFrom.contains=" + UPDATED_CURRENCY_FROM);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByCurrencyFromNotContainsSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where currencyFrom does not contain DEFAULT_CURRENCY_FROM
        defaultExchangeValueShouldNotBeFound("currencyFrom.doesNotContain=" + DEFAULT_CURRENCY_FROM);

        // Get all the exchangeValueList where currencyFrom does not contain UPDATED_CURRENCY_FROM
        defaultExchangeValueShouldBeFound("currencyFrom.doesNotContain=" + UPDATED_CURRENCY_FROM);
    }


    @Test
    @Transactional
    public void getAllExchangeValuesByCurrencyToIsEqualToSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where currencyTo equals to DEFAULT_CURRENCY_TO
        defaultExchangeValueShouldBeFound("currencyTo.equals=" + DEFAULT_CURRENCY_TO);

        // Get all the exchangeValueList where currencyTo equals to UPDATED_CURRENCY_TO
        defaultExchangeValueShouldNotBeFound("currencyTo.equals=" + UPDATED_CURRENCY_TO);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByCurrencyToIsNotEqualToSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where currencyTo not equals to DEFAULT_CURRENCY_TO
        defaultExchangeValueShouldNotBeFound("currencyTo.notEquals=" + DEFAULT_CURRENCY_TO);

        // Get all the exchangeValueList where currencyTo not equals to UPDATED_CURRENCY_TO
        defaultExchangeValueShouldBeFound("currencyTo.notEquals=" + UPDATED_CURRENCY_TO);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByCurrencyToIsInShouldWork() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where currencyTo in DEFAULT_CURRENCY_TO or UPDATED_CURRENCY_TO
        defaultExchangeValueShouldBeFound("currencyTo.in=" + DEFAULT_CURRENCY_TO + "," + UPDATED_CURRENCY_TO);

        // Get all the exchangeValueList where currencyTo equals to UPDATED_CURRENCY_TO
        defaultExchangeValueShouldNotBeFound("currencyTo.in=" + UPDATED_CURRENCY_TO);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByCurrencyToIsNullOrNotNull() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where currencyTo is not null
        defaultExchangeValueShouldBeFound("currencyTo.specified=true");

        // Get all the exchangeValueList where currencyTo is null
        defaultExchangeValueShouldNotBeFound("currencyTo.specified=false");
    }
                @Test
    @Transactional
    public void getAllExchangeValuesByCurrencyToContainsSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where currencyTo contains DEFAULT_CURRENCY_TO
        defaultExchangeValueShouldBeFound("currencyTo.contains=" + DEFAULT_CURRENCY_TO);

        // Get all the exchangeValueList where currencyTo contains UPDATED_CURRENCY_TO
        defaultExchangeValueShouldNotBeFound("currencyTo.contains=" + UPDATED_CURRENCY_TO);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByCurrencyToNotContainsSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where currencyTo does not contain DEFAULT_CURRENCY_TO
        defaultExchangeValueShouldNotBeFound("currencyTo.doesNotContain=" + DEFAULT_CURRENCY_TO);

        // Get all the exchangeValueList where currencyTo does not contain UPDATED_CURRENCY_TO
        defaultExchangeValueShouldBeFound("currencyTo.doesNotContain=" + UPDATED_CURRENCY_TO);
    }


    @Test
    @Transactional
    public void getAllExchangeValuesByConversionMultipleIsEqualToSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where conversionMultiple equals to DEFAULT_CONVERSION_MULTIPLE
        defaultExchangeValueShouldBeFound("conversionMultiple.equals=" + DEFAULT_CONVERSION_MULTIPLE);

        // Get all the exchangeValueList where conversionMultiple equals to UPDATED_CONVERSION_MULTIPLE
        defaultExchangeValueShouldNotBeFound("conversionMultiple.equals=" + UPDATED_CONVERSION_MULTIPLE);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByConversionMultipleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where conversionMultiple not equals to DEFAULT_CONVERSION_MULTIPLE
        defaultExchangeValueShouldNotBeFound("conversionMultiple.notEquals=" + DEFAULT_CONVERSION_MULTIPLE);

        // Get all the exchangeValueList where conversionMultiple not equals to UPDATED_CONVERSION_MULTIPLE
        defaultExchangeValueShouldBeFound("conversionMultiple.notEquals=" + UPDATED_CONVERSION_MULTIPLE);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByConversionMultipleIsInShouldWork() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where conversionMultiple in DEFAULT_CONVERSION_MULTIPLE or UPDATED_CONVERSION_MULTIPLE
        defaultExchangeValueShouldBeFound("conversionMultiple.in=" + DEFAULT_CONVERSION_MULTIPLE + "," + UPDATED_CONVERSION_MULTIPLE);

        // Get all the exchangeValueList where conversionMultiple equals to UPDATED_CONVERSION_MULTIPLE
        defaultExchangeValueShouldNotBeFound("conversionMultiple.in=" + UPDATED_CONVERSION_MULTIPLE);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByConversionMultipleIsNullOrNotNull() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where conversionMultiple is not null
        defaultExchangeValueShouldBeFound("conversionMultiple.specified=true");

        // Get all the exchangeValueList where conversionMultiple is null
        defaultExchangeValueShouldNotBeFound("conversionMultiple.specified=false");
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByConversionMultipleIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where conversionMultiple is greater than or equal to DEFAULT_CONVERSION_MULTIPLE
        defaultExchangeValueShouldBeFound("conversionMultiple.greaterThanOrEqual=" + DEFAULT_CONVERSION_MULTIPLE);

        // Get all the exchangeValueList where conversionMultiple is greater than or equal to UPDATED_CONVERSION_MULTIPLE
        defaultExchangeValueShouldNotBeFound("conversionMultiple.greaterThanOrEqual=" + UPDATED_CONVERSION_MULTIPLE);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByConversionMultipleIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where conversionMultiple is less than or equal to DEFAULT_CONVERSION_MULTIPLE
        defaultExchangeValueShouldBeFound("conversionMultiple.lessThanOrEqual=" + DEFAULT_CONVERSION_MULTIPLE);

        // Get all the exchangeValueList where conversionMultiple is less than or equal to SMALLER_CONVERSION_MULTIPLE
        defaultExchangeValueShouldNotBeFound("conversionMultiple.lessThanOrEqual=" + SMALLER_CONVERSION_MULTIPLE);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByConversionMultipleIsLessThanSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where conversionMultiple is less than DEFAULT_CONVERSION_MULTIPLE
        defaultExchangeValueShouldNotBeFound("conversionMultiple.lessThan=" + DEFAULT_CONVERSION_MULTIPLE);

        // Get all the exchangeValueList where conversionMultiple is less than UPDATED_CONVERSION_MULTIPLE
        defaultExchangeValueShouldBeFound("conversionMultiple.lessThan=" + UPDATED_CONVERSION_MULTIPLE);
    }

    @Test
    @Transactional
    public void getAllExchangeValuesByConversionMultipleIsGreaterThanSomething() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        // Get all the exchangeValueList where conversionMultiple is greater than DEFAULT_CONVERSION_MULTIPLE
        defaultExchangeValueShouldNotBeFound("conversionMultiple.greaterThan=" + DEFAULT_CONVERSION_MULTIPLE);

        // Get all the exchangeValueList where conversionMultiple is greater than SMALLER_CONVERSION_MULTIPLE
        defaultExchangeValueShouldBeFound("conversionMultiple.greaterThan=" + SMALLER_CONVERSION_MULTIPLE);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultExchangeValueShouldBeFound(String filter) throws Exception {
        restExchangeValueMockMvc.perform(get("/api/exchange-values?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exchangeValue.getId().intValue())))
            .andExpect(jsonPath("$.[*].currencyFrom").value(hasItem(DEFAULT_CURRENCY_FROM)))
            .andExpect(jsonPath("$.[*].currencyTo").value(hasItem(DEFAULT_CURRENCY_TO)))
            .andExpect(jsonPath("$.[*].conversionMultiple").value(hasItem(DEFAULT_CONVERSION_MULTIPLE.intValue())));

        // Check, that the count call also returns 1
        restExchangeValueMockMvc.perform(get("/api/exchange-values/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultExchangeValueShouldNotBeFound(String filter) throws Exception {
        restExchangeValueMockMvc.perform(get("/api/exchange-values?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restExchangeValueMockMvc.perform(get("/api/exchange-values/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingExchangeValue() throws Exception {
        // Get the exchangeValue
        restExchangeValueMockMvc.perform(get("/api/exchange-values/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExchangeValue() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        int databaseSizeBeforeUpdate = exchangeValueRepository.findAll().size();

        // Update the exchangeValue
        ExchangeValue updatedExchangeValue = exchangeValueRepository.findById(exchangeValue.getId()).get();
        // Disconnect from session so that the updates on updatedExchangeValue are not directly saved in db
        em.detach(updatedExchangeValue);
        updatedExchangeValue
            .currencyFrom(UPDATED_CURRENCY_FROM)
            .currencyTo(UPDATED_CURRENCY_TO)
            .conversionMultiple(UPDATED_CONVERSION_MULTIPLE);
        ExchangeValueDTO exchangeValueDTO = exchangeValueMapper.toDto(updatedExchangeValue);

        restExchangeValueMockMvc.perform(put("/api/exchange-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(exchangeValueDTO)))
            .andExpect(status().isOk());

        // Validate the ExchangeValue in the database
        List<ExchangeValue> exchangeValueList = exchangeValueRepository.findAll();
        assertThat(exchangeValueList).hasSize(databaseSizeBeforeUpdate);
        ExchangeValue testExchangeValue = exchangeValueList.get(exchangeValueList.size() - 1);
        assertThat(testExchangeValue.getCurrencyFrom()).isEqualTo(UPDATED_CURRENCY_FROM);
        assertThat(testExchangeValue.getCurrencyTo()).isEqualTo(UPDATED_CURRENCY_TO);
        assertThat(testExchangeValue.getConversionMultiple()).isEqualTo(UPDATED_CONVERSION_MULTIPLE);
    }

    @Test
    @Transactional
    public void updateNonExistingExchangeValue() throws Exception {
        int databaseSizeBeforeUpdate = exchangeValueRepository.findAll().size();

        // Create the ExchangeValue
        ExchangeValueDTO exchangeValueDTO = exchangeValueMapper.toDto(exchangeValue);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangeValueMockMvc.perform(put("/api/exchange-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(exchangeValueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ExchangeValue in the database
        List<ExchangeValue> exchangeValueList = exchangeValueRepository.findAll();
        assertThat(exchangeValueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteExchangeValue() throws Exception {
        // Initialize the database
        exchangeValueRepository.saveAndFlush(exchangeValue);

        int databaseSizeBeforeDelete = exchangeValueRepository.findAll().size();

        // Delete the exchangeValue
        restExchangeValueMockMvc.perform(delete("/api/exchange-values/{id}", exchangeValue.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExchangeValue> exchangeValueList = exchangeValueRepository.findAll();
        assertThat(exchangeValueList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
