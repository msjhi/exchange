package sn.mara.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A ExchangeValue.
 */
@Entity
@Table(name = "exchange_value")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ExchangeValue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 255)
    @Column(name = "currency_from", length = 255)
    private String currencyFrom;

    @Size(max = 255)
    @Column(name = "currency_to", length = 255)
    private String currencyTo;

    @Column(name = "conversion_multiple", precision = 21, scale = 2)
    private BigDecimal conversionMultiple;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrencyFrom() {
        return currencyFrom;
    }

    public ExchangeValue currencyFrom(String currencyFrom) {
        this.currencyFrom = currencyFrom;
        return this;
    }

    public void setCurrencyFrom(String currencyFrom) {
        this.currencyFrom = currencyFrom;
    }

    public String getCurrencyTo() {
        return currencyTo;
    }

    public ExchangeValue currencyTo(String currencyTo) {
        this.currencyTo = currencyTo;
        return this;
    }

    public void setCurrencyTo(String currencyTo) {
        this.currencyTo = currencyTo;
    }

    public BigDecimal getConversionMultiple() {
        return conversionMultiple;
    }

    public ExchangeValue conversionMultiple(BigDecimal conversionMultiple) {
        this.conversionMultiple = conversionMultiple;
        return this;
    }

    public void setConversionMultiple(BigDecimal conversionMultiple) {
        this.conversionMultiple = conversionMultiple;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExchangeValue)) {
            return false;
        }
        return id != null && id.equals(((ExchangeValue) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ExchangeValue{" +
            "id=" + getId() +
            ", currencyFrom='" + getCurrencyFrom() + "'" +
            ", currencyTo='" + getCurrencyTo() + "'" +
            ", conversionMultiple=" + getConversionMultiple() +
            "}";
    }
}
