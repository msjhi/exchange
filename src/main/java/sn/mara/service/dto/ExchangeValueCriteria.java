package sn.mara.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link sn.mara.domain.ExchangeValue} entity. This class is used
 * in {@link sn.mara.web.rest.ExchangeValueResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /exchange-values?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ExchangeValueCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter currencyFrom;

    private StringFilter currencyTo;

    private BigDecimalFilter conversionMultiple;

    public ExchangeValueCriteria(){
    }

    public ExchangeValueCriteria(ExchangeValueCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.currencyFrom = other.currencyFrom == null ? null : other.currencyFrom.copy();
        this.currencyTo = other.currencyTo == null ? null : other.currencyTo.copy();
        this.conversionMultiple = other.conversionMultiple == null ? null : other.conversionMultiple.copy();
    }

    @Override
    public ExchangeValueCriteria copy() {
        return new ExchangeValueCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCurrencyFrom() {
        return currencyFrom;
    }

    public void setCurrencyFrom(StringFilter currencyFrom) {
        this.currencyFrom = currencyFrom;
    }

    public StringFilter getCurrencyTo() {
        return currencyTo;
    }

    public void setCurrencyTo(StringFilter currencyTo) {
        this.currencyTo = currencyTo;
    }

    public BigDecimalFilter getConversionMultiple() {
        return conversionMultiple;
    }

    public void setConversionMultiple(BigDecimalFilter conversionMultiple) {
        this.conversionMultiple = conversionMultiple;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ExchangeValueCriteria that = (ExchangeValueCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(currencyFrom, that.currencyFrom) &&
            Objects.equals(currencyTo, that.currencyTo) &&
            Objects.equals(conversionMultiple, that.conversionMultiple);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        currencyFrom,
        currencyTo,
        conversionMultiple
        );
    }

    @Override
    public String toString() {
        return "ExchangeValueCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (currencyFrom != null ? "currencyFrom=" + currencyFrom + ", " : "") +
                (currencyTo != null ? "currencyTo=" + currencyTo + ", " : "") +
                (conversionMultiple != null ? "conversionMultiple=" + conversionMultiple + ", " : "") +
            "}";
    }

}
