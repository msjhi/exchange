package sn.mara.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link sn.mara.domain.ExchangeValue} entity.
 */
public class ExchangeValueDTO implements Serializable {

    private Long id;

    @Size(max = 255)
    private String currencyFrom;

    @Size(max = 255)
    private String currencyTo;

    private BigDecimal conversionMultiple;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrencyFrom() {
        return currencyFrom;
    }

    public void setCurrencyFrom(String currencyFrom) {
        this.currencyFrom = currencyFrom;
    }

    public String getCurrencyTo() {
        return currencyTo;
    }

    public void setCurrencyTo(String currencyTo) {
        this.currencyTo = currencyTo;
    }

    public BigDecimal getConversionMultiple() {
        return conversionMultiple;
    }

    public void setConversionMultiple(BigDecimal conversionMultiple) {
        this.conversionMultiple = conversionMultiple;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ExchangeValueDTO exchangeValueDTO = (ExchangeValueDTO) o;
        if (exchangeValueDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), exchangeValueDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ExchangeValueDTO{" +
            "id=" + getId() +
            ", currencyFrom='" + getCurrencyFrom() + "'" +
            ", currencyTo='" + getCurrencyTo() + "'" +
            ", conversionMultiple=" + getConversionMultiple() +
            "}";
    }
}
