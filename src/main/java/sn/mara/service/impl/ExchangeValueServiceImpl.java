package sn.mara.service.impl;

import sn.mara.service.ExchangeValueService;
import sn.mara.domain.ExchangeValue;
import sn.mara.repository.ExchangeValueRepository;
import sn.mara.service.dto.ExchangeValueDTO;
import sn.mara.service.mapper.ExchangeValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ExchangeValue}.
 */
@Service
@Transactional
public class ExchangeValueServiceImpl implements ExchangeValueService {

    private final Logger log = LoggerFactory.getLogger(ExchangeValueServiceImpl.class);

    private final ExchangeValueRepository exchangeValueRepository;

    private final ExchangeValueMapper exchangeValueMapper;

    public ExchangeValueServiceImpl(ExchangeValueRepository exchangeValueRepository, ExchangeValueMapper exchangeValueMapper) {
        this.exchangeValueRepository = exchangeValueRepository;
        this.exchangeValueMapper = exchangeValueMapper;
    }

    /**
     * Save a exchangeValue.
     *
     * @param exchangeValueDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ExchangeValueDTO save(ExchangeValueDTO exchangeValueDTO) {
        log.debug("Request to save ExchangeValue : {}", exchangeValueDTO);
        ExchangeValue exchangeValue = exchangeValueMapper.toEntity(exchangeValueDTO);
        exchangeValue = exchangeValueRepository.save(exchangeValue);
        return exchangeValueMapper.toDto(exchangeValue);
    }

    /**
     * Get all the exchangeValues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ExchangeValueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ExchangeValues");
        return exchangeValueRepository.findAll(pageable)
            .map(exchangeValueMapper::toDto);
    }


    /**
     * Get one exchangeValue by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ExchangeValueDTO> findOne(Long id) {
        log.debug("Request to get ExchangeValue : {}", id);
        return exchangeValueRepository.findById(id)
            .map(exchangeValueMapper::toDto);
    }

    /**
     * Delete the exchangeValue by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ExchangeValue : {}", id);
        exchangeValueRepository.deleteById(id);
    }
}
