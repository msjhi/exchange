package sn.mara.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import sn.mara.domain.ExchangeValue;
import sn.mara.domain.*; // for static metamodels
import sn.mara.repository.ExchangeValueRepository;
import sn.mara.service.dto.ExchangeValueCriteria;
import sn.mara.service.dto.ExchangeValueDTO;
import sn.mara.service.mapper.ExchangeValueMapper;

/**
 * Service for executing complex queries for {@link ExchangeValue} entities in the database.
 * The main input is a {@link ExchangeValueCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ExchangeValueDTO} or a {@link Page} of {@link ExchangeValueDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ExchangeValueQueryService extends QueryService<ExchangeValue> {

    private final Logger log = LoggerFactory.getLogger(ExchangeValueQueryService.class);

    private final ExchangeValueRepository exchangeValueRepository;

    private final ExchangeValueMapper exchangeValueMapper;

    public ExchangeValueQueryService(ExchangeValueRepository exchangeValueRepository, ExchangeValueMapper exchangeValueMapper) {
        this.exchangeValueRepository = exchangeValueRepository;
        this.exchangeValueMapper = exchangeValueMapper;
    }

    /**
     * Return a {@link List} of {@link ExchangeValueDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ExchangeValueDTO> findByCriteria(ExchangeValueCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ExchangeValue> specification = createSpecification(criteria);
        return exchangeValueMapper.toDto(exchangeValueRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ExchangeValueDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ExchangeValueDTO> findByCriteria(ExchangeValueCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ExchangeValue> specification = createSpecification(criteria);
        return exchangeValueRepository.findAll(specification, page)
            .map(exchangeValueMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ExchangeValueCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ExchangeValue> specification = createSpecification(criteria);
        return exchangeValueRepository.count(specification);
    }

    /**
     * Function to convert {@link ExchangeValueCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ExchangeValue> createSpecification(ExchangeValueCriteria criteria) {
        Specification<ExchangeValue> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ExchangeValue_.id));
            }
            if (criteria.getCurrencyFrom() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCurrencyFrom(), ExchangeValue_.currencyFrom));
            }
            if (criteria.getCurrencyTo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCurrencyTo(), ExchangeValue_.currencyTo));
            }
            if (criteria.getConversionMultiple() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getConversionMultiple(), ExchangeValue_.conversionMultiple));
            }
        }
        return specification;
    }
}
