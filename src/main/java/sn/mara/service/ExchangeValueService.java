package sn.mara.service;

import sn.mara.service.dto.ExchangeValueDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.mara.domain.ExchangeValue}.
 */
public interface ExchangeValueService {

    /**
     * Save a exchangeValue.
     *
     * @param exchangeValueDTO the entity to save.
     * @return the persisted entity.
     */
    ExchangeValueDTO save(ExchangeValueDTO exchangeValueDTO);

    /**
     * Get all the exchangeValues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExchangeValueDTO> findAll(Pageable pageable);


    /**
     * Get the "id" exchangeValue.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExchangeValueDTO> findOne(Long id);

    /**
     * Delete the "id" exchangeValue.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
