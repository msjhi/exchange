package sn.mara.service.mapper;

import sn.mara.domain.*;
import sn.mara.service.dto.ExchangeValueDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ExchangeValue} and its DTO {@link ExchangeValueDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ExchangeValueMapper extends EntityMapper<ExchangeValueDTO, ExchangeValue> {



    default ExchangeValue fromId(Long id) {
        if (id == null) {
            return null;
        }
        ExchangeValue exchangeValue = new ExchangeValue();
        exchangeValue.setId(id);
        return exchangeValue;
    }
}
