package sn.mara.repository;

import sn.mara.domain.ExchangeValue;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ExchangeValue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExchangeValueRepository extends JpaRepository<ExchangeValue, Long>, JpaSpecificationExecutor<ExchangeValue> {

}
