package sn.mara.web.rest;

import sn.mara.service.ExchangeValueService;
import sn.mara.web.rest.errors.BadRequestAlertException;
import sn.mara.service.dto.ExchangeValueDTO;
import sn.mara.service.dto.ExchangeValueCriteria;
import sn.mara.service.ExchangeValueQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.mara.domain.ExchangeValue}.
 */
@RestController
@RequestMapping("/api")
public class ExchangeValueResource {

    private final Logger log = LoggerFactory.getLogger(ExchangeValueResource.class);

    private static final String ENTITY_NAME = "exchangeValuesExchangeValue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExchangeValueService exchangeValueService;

    private final ExchangeValueQueryService exchangeValueQueryService;

    public ExchangeValueResource(ExchangeValueService exchangeValueService, ExchangeValueQueryService exchangeValueQueryService) {
        this.exchangeValueService = exchangeValueService;
        this.exchangeValueQueryService = exchangeValueQueryService;
    }

    /**
     * {@code POST  /exchange-values} : Create a new exchangeValue.
     *
     * @param exchangeValueDTO the exchangeValueDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new exchangeValueDTO, or with status {@code 400 (Bad Request)} if the exchangeValue has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/exchange-values")
    public ResponseEntity<ExchangeValueDTO> createExchangeValue(@Valid @RequestBody ExchangeValueDTO exchangeValueDTO) throws URISyntaxException {
        log.debug("REST request to save ExchangeValue : {}", exchangeValueDTO);
        if (exchangeValueDTO.getId() != null) {
            throw new BadRequestAlertException("A new exchangeValue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExchangeValueDTO result = exchangeValueService.save(exchangeValueDTO);
        return ResponseEntity.created(new URI("/api/exchange-values/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /exchange-values} : Updates an existing exchangeValue.
     *
     * @param exchangeValueDTO the exchangeValueDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated exchangeValueDTO,
     * or with status {@code 400 (Bad Request)} if the exchangeValueDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the exchangeValueDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/exchange-values")
    public ResponseEntity<ExchangeValueDTO> updateExchangeValue(@Valid @RequestBody ExchangeValueDTO exchangeValueDTO) throws URISyntaxException {
        log.debug("REST request to update ExchangeValue : {}", exchangeValueDTO);
        if (exchangeValueDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ExchangeValueDTO result = exchangeValueService.save(exchangeValueDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, exchangeValueDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /exchange-values} : get all the exchangeValues.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of exchangeValues in body.
     */
    @GetMapping("/exchange-values")
    public ResponseEntity<List<ExchangeValueDTO>> getAllExchangeValues(ExchangeValueCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ExchangeValues by criteria: {}", criteria);
        Page<ExchangeValueDTO> page = exchangeValueQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /exchange-values/count} : count all the exchangeValues.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/exchange-values/count")
    public ResponseEntity<Long> countExchangeValues(ExchangeValueCriteria criteria) {
        log.debug("REST request to count ExchangeValues by criteria: {}", criteria);
        return ResponseEntity.ok().body(exchangeValueQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /exchange-values/:id} : get the "id" exchangeValue.
     *
     * @param id the id of the exchangeValueDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the exchangeValueDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/exchange-values/{id}")
    public ResponseEntity<ExchangeValueDTO> getExchangeValue(@PathVariable Long id) {
        log.debug("REST request to get ExchangeValue : {}", id);
        Optional<ExchangeValueDTO> exchangeValueDTO = exchangeValueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(exchangeValueDTO);
    }

    /**
     * {@code DELETE  /exchange-values/:id} : delete the "id" exchangeValue.
     *
     * @param id the id of the exchangeValueDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/exchange-values/{id}")
    public ResponseEntity<Void> deleteExchangeValue(@PathVariable Long id) {
        log.debug("REST request to delete ExchangeValue : {}", id);
        exchangeValueService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
